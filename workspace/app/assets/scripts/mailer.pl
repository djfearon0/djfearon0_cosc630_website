#!/usr/bin/perl

#Dakota Fearon
#COSC 630
#6/30/16

use strict;
use warnings;
#The modules imported below are required for functionality
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP::TLS;
use Email::Valid;

my $host = 'smtp.gmail.com'; #Setup to use Gmail
my $user = 'djfearondev'; #Gmail username
my $pass = 'NothingToSay94'; #Gmail Password

my $email = $ARGV[0];
my $subject = $ARGV[1];
my $message = $ARGV[2];

if( !Email::Valid->address($email) ){
    print "emailError";
    exit 1;
}

my $to = 'djfearon0@frostburg.edu';#The receiving email for the website owner
my $from = 'djfearondev@gmail.com';#The email that the message will be routed through

if( $ARGV[2] ne '' && $ARGV[1] ne '' && $ARGV[0] ne '' ) { # no warning

    my $sender = Email::Sender::Transport::SMTP::TLS->new(
        host => $host,
        port => 587,
        username => $user,
        password => $pass,
        helo => 'jazzsaxophone.com'
        );

    my $msg = Email::Simple->create(
        header => [
        From => $from,
        To => $to,
        Subject => $subject
        ],
        body => "Respond To: " . $email . "\n\n\n" . "***************MESSAGE***************\n\n" . $message,
        );

    try{
        sendmail($msg, {transport => $sender});
        print "true";
        return;
    } catch{
        print "false";
    }
}
else{
    print "error";
    exit 1;
}