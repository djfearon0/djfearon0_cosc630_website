function ajax(route, target, data) {
    var path = route;
    var div = document.getElementById(target);
    if(!data){
        data = "";
    }
    $.ajax({
        method: 'POST',
        url: path,
        data: {'email': data[0], 'subject': data[1], 'message': data[2]},
        success: function(result) {
            $(div).html(result);
        }
    });
}

function clearForm(){
    document.getElementById("mailForm").reset();
    document.getElementById("notifications").innerHTML = "";
}

function mailer(){
    var email = document.getElementById("mailTo").value;
    var subject = document.getElementById("mailSubject").value;
    var message = document.getElementById("mailBody").value;
    var messageData = [email, subject, message];
    ajax('/mailer', 'notifications', messageData);
}

function getBio(name){

    var nameDiv = document.getElementById("name");
    var nicknameDiv = document.getElementById("nicknames");
    var imageDiv = document.getElementById("image");
    var birthDiv = document.getElementById("birth");
    var deathDiv = document.getElementById("death");
    var bioDiv = document.getElementById("bio");
    var urlDiv = document.getElementById("url");

    var searchVal = name;

    $.getJSON('/json/bios.json', function(data) {
        $.each(data, function(key, val) {
            if (val.name.includes(searchVal)) {
                nameDiv.innerHTML = val.name;
                if(val.nicknames != ""){
                    nicknameDiv.innerHTML = val.nicknames;
                } else {
                    nicknameDiv.innerHTML = "N/A";
                }
                imageDiv.innerHTML = val.image;
                birthDiv.innerHTML = val.birth;
                deathDiv.innerHTML = val.death;
                bioDiv.innerHTML = val.bio;
                urlDiv.innerHTML = val.url;
            }
        });
    });
}