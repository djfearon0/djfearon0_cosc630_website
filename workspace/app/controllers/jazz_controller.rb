class JazzController < ApplicationController
    
    def index
    end
    
    def info
    end
    
    def resources
    end
    
    def artists
    end

    def mailer
        em = '"' + params[:email] + '"'
        sub = '"' + params[:subject] + '"'
        msg = '"' + params[:message] + '"'
        file = File.join(Rails.root, 'app', 'assets', 'scripts', 'mailer.pl')
        path = "#{File.absolute_path(file)}"
        cmd = `perl #{path} #{em} #{sub} #{msg}`
        puts cmd
        if cmd.eql? 'true'
            render :inline => "<div class='alert alert-success'>
                                <strong>Your message has been sent!</strong>
                              </div>".html_safe
        elsif cmd.eql? 'error'
            render :inline => "<div class='alert alert-danger'>
                                <strong>Please review the form and fill in all fields.</strong>
                              </div>".html_safe
        elsif cmd.eql? 'emailError'
            render :inline => "<div class='alert alert-danger'>
                                <strong>Please enter a vaild email.</strong>
                              </div>".html_safe
        else
            render :inline => "<div class='alert alert-danger'>
                                <strong>There was a problem! Try again later or try emailing djfearon0@frostburg.edu using another service.</strong>
                              </div>".html_safe
        end
    end

end
